package com.tianpengtech.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerUtils {


	public static void outPutPage(String loadDir,String template,String outPath,Map<String,Object>params){
		Configuration cfg = new Configuration();
    	try {
			cfg.setDirectoryForTemplateLoading(new File(loadDir));
			cfg.setObjectWrapper(new DefaultObjectWrapper());  
	        Template temp = cfg.getTemplate(template);
	        String outPutPath= outPath;
	        File file = new File(outPutPath);
	        
	        if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
	        }
	        if(!file.exists()){
	        	file.createNewFile();
	        }
	        
	        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPutPath)));
	        temp.process(params, out);  
	        out.flush();    
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}  
        
	}
}

