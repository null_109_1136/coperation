package com.tianpengtech.util;

import com.mysql.jdbc.StringUtils;

public class ValidateUtil {

	public String trim(String value){

		boolean r = StringUtils.isEmptyOrWhitespaceOnly(value);
		if(r){
			return value;
		}else{
			value = value.trim();	
		}
		return value;
	}
}

