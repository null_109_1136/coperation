package com.tianpengtech.util;

import java.util.HashMap;
import java.util.Map;

public class JsonUtil {

	
	public static Map<String,Object> printSuccess(String msg){
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("result", 1);
		map.put("msg",msg);
		return map;
	}
	
	public static Map<String,Object> printError(String msg){
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("result", 0);
		map.put("msg",msg);
		return map;
	}
	
	public static Map<String,Object> printObject(Object obj){
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("result", obj);
		return map;
	}
}
