package com.tianpengtech.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RexUtil {

	private final static String  EMAIL="^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
	private final static String  MOBILE="^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
	private final static String  PASSWORD="^[0-9a-zA-Z]{6,16}$";
	
	private final static String  DATE="(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)";
	
	public static boolean isUserId(String value){
		
		boolean isEmail = isEmail(value);
		boolean isMobile = isMobile(value);
		if(isEmail || isMobile){
			return true;
		}
		return false;
	}
	
	public static boolean isEmail(String value){
		Pattern regex = Pattern.compile(EMAIL);
		Matcher matcher = regex.matcher(value);
        return matcher.matches();
	}
	
	public static boolean isMobile(String value){
		Pattern regex = Pattern.compile(MOBILE);
		Matcher matcher = regex.matcher(value);
        return matcher.matches();
	}
	
	public static boolean isPassWord(String value){
		
		Pattern regex = Pattern.compile(PASSWORD);
		Matcher matcher = regex.matcher(value);
        return matcher.matches();
	}
	
	public static boolean isDate(String value){
		
		Pattern regex = Pattern.compile(DATE);
		Matcher matcher = regex.matcher(value);
        return matcher.matches();
	}
}
